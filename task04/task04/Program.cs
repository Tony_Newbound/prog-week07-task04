﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task04
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine(jeff(6.25, 9.48));
          
        }
        static string jeff(double num1, double num2)
        {
            var output = $"{num1} + {num2}\n";
            output += $"{num1} - {num2}\n";
            output += $"{num1} / {num2}\n";
            output += $"{num1} * {num2}\n";

            return output;

        }
    }
}
